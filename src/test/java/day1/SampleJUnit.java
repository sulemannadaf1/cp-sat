package day1;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;

class SampleJUnit {
	WebDriver driver;

	@BeforeEach
	void setUp() throws Exception {
		driver = utils.HelperFunctions.createAppropriateDriver("CHROME");
		
	}

	@AfterEach
	void tearDown() throws Exception {
		//driver.close();
	}

	@Test
	void test() {
		driver.get("https://imdb.com");
		System.out.println("actual title: "+driver.getTitle());
		String actual = driver.getTitle();
		String expected = "The Internet";
		Assert.assertEquals(actual, expected);
	}

}
