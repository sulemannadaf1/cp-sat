package exercise;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterTest;

public class Ex07_IMDB_NG {

	WebDriver driver = null;
	// String excelPath = "C:\\Users\\sulemannadaf\\Desktop\\Tools\\sample_excel\\test_excel.xlsx";
	//String sheetName = "IMDB";
	String excelPath = "src\\test\\resources\\data\\imdbdata.xlsx";
	String sheetName = "data";
	

	@BeforeTest
	public void beforeTest() {
		driver = utils.HelperFunctions.createAppropriateDriver("Chrome",false);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.get("https://www.imdb.com/");

	}

	@DataProvider
	public Object[][] dp() throws EncryptedDocumentException, InvalidFormatException, IOException {
		//		String [][] data = {	{"Avengers","Chris Evans", "Joss Whedon" },
		//				{"Raazi", "Vicky Kaushal", "Meghna Gulzar"} };

		String[][] excelData = utils.XLDataReaders.getExcelData(excelPath, sheetName); 
		for (int i=0;i<excelData.length;i++) { 
			System.out.println("excel data is: "+Arrays.toString(excelData[i])); 
			// data[data.length+1] = excelData[i]; 
		} 
		System.out.println("Number of records in excel = "+excelData.length); 
		return excelData;

	}


	@Test (dataProvider = "dp")
	public void testIMDB(String v1, String v2, String v3) { 
		String movie = v1; //"Avengers";
		String expStar = v2 ; //"Chris Evans"; 
		String expDir = v3; //"Joss Whedan";

		//driver.get("https://www.imdb.com/");
		driver.findElement(By.xpath("//*[@id=\"suggestion-search\"]")).sendKeys(movie);
		driver.findElement(By.xpath("//*[@id=\"suggestion-search-button\"]")).click();

		driver.findElement(By.xpath("//*[@id=\"main\"]/div/div[2]/table/tbody/tr[1]/td[2]/a")).click();

		//*[@class="inline" and contains(text(),'Star' )]/following-sibling::* All Stars' siblings

		By byStar = By.xpath("//*[@class=\"inline\" and contains(text(),'Star')]/following-sibling::*");
		List<WebElement> starList = driver.findElements(byStar);


		//Shah Rukh Khan 
		for (WebElement ele : starList) { 
			String actStar = ele.getText(); 
			if (actStar.contains(expStar)) {
				System.out.println("Found Star : "+ actStar ); break;
			}
		}

		By byDir = By.xpath("//*[@class=\"inline\" and contains(text(),'Dir'  )]/following-sibling::*");
		List<WebElement> dirList = driver.findElements(byDir);

		for (WebElement ele1 : starList) {
			String actDir = ele1.getText();
			if	 (actDir.contains(expDir)) { 
				System.out.println("Found Dir : "+ actDir );
				break;
			} 
		}
	}

	//	@Test (dataProvider = "dp")
	//	public void excelData() throws EncryptedDocumentException, InvalidFormatException, IOException {
	//		String[][] data = { { "Avengers", "Chris Evans", "Joss Whedon" },
	//				{ "Raazi", "Vicky Kaushal", "Meghna Gulzar" } };
	//		String[][] xlData = utils.XLDataReaders.getExcelData(excelPath, sheetName);
	//		System.out.println("excel data array size: " + xlData.length);
	//		System.out.println("record length in array is: " + xlData[0].length);
	//		for (int i = 0; i < xlData.length; i++) {
	//
	//			System.out.println("excel data is: " + Arrays.toString(xlData[i]));
	//		}
	//		String[][] sum = new String[(data.length) + xlData.length][(data[0].length)];
	//		System.out.println("sum size is:" + sum.length + " with record size: " + sum[0].length);
	//
	//	}

	/*
	 * @Test public void testMovie() { String movie ="Raazi"; String expStar =
	 * "Vicky Kaushal"; String expDir = "Meghna Gulzar";
	 * 
	 * //driver.get("https://www.imdb.com/");
	 * driver.findElement(By.xpath("//*[@id=\"suggestion-search\"]")).sendKeys(movie
	 * );
	 * driver.findElement(By.xpath("//*[@id=\"suggestion-search-button\"]")).click()
	 * ;
	 * 
	 * driver.findElement(By.xpath(
	 * "//*[@id=\"main\"]/div/div[2]/table/tbody/tr[1]/td[2]/a")).click();
	 * 
	 * 
	 * //*[@class="inline" and contains(text(),'Star' )]/following-sibling::* All
	 * Stars' siblings
	 * 
	 * By byStar = By.
	 * xpath("//*[@class=\"inline\" and contains(text(),'Star'  )]/following-sibling::*"
	 * ); List<WebElement> starList = driver.findElements(byStar);
	 * 
	 * 
	 * //Shah Rukh Khan for (WebElement ele : starList) { String actStar =
	 * ele.getText(); if (actStar.contains(expStar)) {
	 * System.out.println("Found Star : "+ actStar ); break;
	 * 
	 * } }
	 * 
	 * By byDir = By.
	 * xpath("//*[@class='inline' and contains(text(),'Dir'  )]/following-sibling::*"
	 * ); List<WebElement> dirList = driver.findElements(byDir);
	 * 
	 * 
	 * boolean dirFlag = false; for (WebElement ele1 : dirList) { String actDir =
	 * ele1.getText(); if (actDir.contains(expDir)) {
	 * System.out.println("Found Dir : "+ actDir ); break; }
	 * 
	 * }
	 * 
	 * 
	 * 
	 * 
	 * }
	 */

	@AfterTest
	public void afterTest() {

		// driver.quit();

	}

}
