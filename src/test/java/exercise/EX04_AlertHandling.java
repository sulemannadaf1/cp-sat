package exercise;

import java.util.List;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class EX04_AlertHandling {

	public static void main(String[] args) throws InterruptedException {
		String url = "https://the-internet.herokuapp.com/javascript_alerts";
		WebDriver driver = utils.HelperFunctions.createAppropriateDriver("CHROME");
		driver.navigate().to(url);
		
		By jsAlert = By.xpath("//*[@id=\"content\"]/div/ul/li[*]/button");

		String result = driver.findElement(By.xpath("//*[@id=\"result\"]")).getText();
		
		WebElement jsAlertButn = driver.findElement(jsAlert);
		jsAlertButn.click();
		//Alert is present only after clicking the button , hence using wait Here
		//taking help of WebDriverWait in order wait until the alert is visible, wait max 10 sec
		WebDriverWait wait = new WebDriverWait(driver, 5);
		wait.until(ExpectedConditions.alertIsPresent());
		System.out.println(driver.switchTo().alert().getText());
		driver.switchTo().alert().accept();
		System.out.println("result of JS Alert is: "+result);
		
		WebElement jSConfirm = driver.findElement(By.xpath("//*[@id=\"content\"]/div/ul/li[2]/button"));
		jSConfirm.click();
		System.out.println(driver.switchTo().alert().getText());
		driver.switchTo().alert().dismiss();
		System.out.println("result of JS Confirm is: "+result);
		
		WebElement jSPrompt = driver.findElement(By.xpath("//*[@id=\"content\"]/div/ul/li[3]/button"));
		jSPrompt.click();
		System.out.println(driver.switchTo().alert().getText());
		driver.switchTo().alert().accept();
		System.out.println("result of JS Prompt is: "+result);
		

	}

}
