package exercise;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class EX03HoverOver_example {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		WebDriver driver = utils.HelperFunctions.createAppropriateDriver("CHROME");
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.get("http://www.annauniv.edu/department/index.php");
		Actions action = new Actions(driver);
		By var = By.xpath("//*[@id=\"link3\"]/strong");
		// -- //*[@id="menuItemHilite32"] :institute of ocean management
		WebElement weiom = driver.findElement(By.id("menuItemHilite32"));
		WebElement target = driver.findElement(var);
		action.moveToElement(target).moveToElement(weiom).click().perform();
		System.out.println("page Title: "+ driver.getTitle());
}
}
