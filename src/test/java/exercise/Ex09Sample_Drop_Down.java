package exercise;

import org.testng.annotations.Test;
import org.testng.annotations.DataProvider;
import org.testng.annotations.BeforeClass;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.AfterClass;

public class Ex09Sample_Drop_Down {
	WebDriver driver;
	
 
  @DataProvider
  public Object[][] dp() {
	  String [][] excelData = null ;
	  
    return excelData;
  }
  @BeforeClass
  public void beforeClass() {
	  driver = utils.HelperFunctions.createAppropriateDriver("chrome");
	  driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
  }
  
 // @Test(dataProvider = "dp")
  @Test
  public void dropDownSelector () {
	  
	  driver.get("file:///C:/Users/sulemannadaf/eclipse-workspace/CPSATJuly/src/test/resources/data/dropdown.html");
	  WebElement drop = driver.findElement(By.name("productId"));
	  
	  Select dropDown = new Select(drop);
	  List<WebElement> dropEle = dropDown.getOptions();
	  System.out.println("total elements in dropDown : "+dropEle.size());
	  for (WebElement ele : dropEle) {
		  System.out.println("element : "+ele.getText());
		  if (ele.getText().contentEquals("CP-MLDS")) {
			  System.out.println("found CP-MLDS");
			  ele.click();
			  break;
		  }
		  
	  }
	  
  }


  @AfterClass
  public void afterClass() throws InterruptedException {

	  driver.quit();
  }

}
