package exercise;

import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

class EX02B_Window_Switching {
	WebDriver driver;
	
	@BeforeEach
	void setUp() throws Exception {
		driver = utils.HelperFunctions.createAppropriateDriver("firefox");
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	}

	@AfterEach
	void tearDown() throws Exception {
	}

	@Test
	void test() throws InterruptedException {

		driver.get("https://www.ataevents.org/");
		//Thread.sleep(5000);
		//By var = By.xpath("//*[@id=\"post-18\"]/div/div/div/div/div/section[3]/div/div/div/div/div/div/div/div/figure/a/img");
		By var = By.xpath("//*[@id=\"post-18\"]/div/div/div/div/div/section[3]/div/div/div[*]/div/div/div/div/div/figure/a");
		List <WebElement> listElements = driver.findElements(var);
		for (WebElement element : listElements) {
			element.click();
		}
		
		Set<String> allHandles = driver.getWindowHandles();
		int noOfWindows = allHandles.size();
		System.out.println("Number of Windows open :"+allHandles.size());
		for (String window : allHandles) {
			driver.switchTo().window(window);
			System.out.println(driver.getTitle());
			if (driver.getTitle().contains("CP-DOF")) {
				System.out.println("page found, exiting...");
				break;
			}
		}
		
//		
//		for (int i=0; i<noOfWindows; i++) {
//			utils.HelperFunctions.switchToWindowbyNum(driver, i);			
//		}
	}
}
