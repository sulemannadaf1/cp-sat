package exercise;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeClass;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterClass;

public class EX_11JQery_UIFrame {
	
	WebDriver driver;
	
	String url ="https://jqueryui.com/autocomplete/";
  @Test
  public void f() {
	  WebElement iFrame = driver.findElement(By.xpath("//*[@id=\"content\"]/iframe"));
	  
	  //we First need to switch to the iFrame
	  driver.switchTo().frame(iFrame);
	  
	  //Then within the iFRame go to the text box, you cannot go to it without switching to the iFrame first.
	  WebElement tag = driver.findElement(By.className("ui-autocomplete-input")); //driver.findElement(By.xpath("//*[@id=\"tags\"]")); 
	  tag.click();
	  tag.sendKeys("J");
	 
	  // once I type in J, i will wait for the results to populate in the iFrame
	 WebDriverWait wait = new WebDriverWait(driver, 10);
	 wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.xpath("//*[@id=\"ui-id-1\"]/li"))); //Anand has used - /html/body/ul/li
	 
	 //after populating, just collect the elements from the result in a List
	 List<WebElement> weEle = driver.findElements(By.xpath("//*[@id=\"ui-id-1\"]/li"));
	 for (WebElement ele : weEle) {
		 System.out.println("element : "+ele.getText());
		 if (ele.getText().equals("Java")) {
			 ele.click();
		 }
	 }
	  
  }
  
  
  @BeforeClass
  public void beforeClass() {
	  System.setProperty("webdriver.chrome.driver", "src\\test\\resources\\drivers\\chromedriver.exe");
	  driver = new ChromeDriver();
	  driver.get(url);
	  driver.manage().window().maximize();
	  driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
  }

  @AfterClass
  public void afterClass() {
  }

}
