package exercise;

import static org.junit.jupiter.api.Assertions.*;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

class EX07_IMDBSearch {
	WebDriver driver ;

	@BeforeEach
	void setUp() throws Exception {
		driver = utils.HelperFunctions.createAppropriateDriver("CHROME");
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		}

	@AfterEach
	void tearDown() throws Exception {
	}

	@Test
	void test() {
		driver.get("https://www.imdb.com/");
		WebElement search = driver.findElement(By.id("suggestion-search"));
		search.sendKeys("Avengers");
		search.sendKeys(Keys.ENTER);
		System.out.println("Result page: "+driver.getTitle());
		WebElement Avengers2012 = driver.findElement(By.xpath("//*[@id=\"main\"]/div/div[2]/table/tbody/tr[1]/td[2]/a"));
		Avengers2012.click();
		//WebElement stars = driver.findElement(By.xpath("//*[@id=\"title-overview-widget\"]/div[2]/div[1]/div[4]"));
		//System.out.println(stars.getText());
		// custom xpath of Directors //*[@class='inline' and contains(text(),'Director')]
		List<WebElement> dir = driver.findElements(By.xpath("//*[@class='inline' and contains(text(),'Director')]/following-sibling::*"));
		System.out.println("Director: "+dir.get(0).getText());
		//xpath for all stars:  //*[@class='inline' and contains(text(),'Star')]/following-sibling::*
		List<WebElement> star = driver.findElements(By.xpath("//*[@class='inline' and contains(text(),'Star')]/following-sibling::*"));
		for (WebElement e : star) {
			System.out.println("Star: "+e.getText());
		}
	}

}
